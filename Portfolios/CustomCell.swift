//
//  CustomCell.swift
//  Portfolios
//
//  Created by mnameit on 12/01/22.
//

import UIKit

class CustomCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var idLabel: UILabel!
    @IBOutlet var currencyLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
}
