//
//  ViewController.swift
//  Portfolios
//
//  Created by mnameit on 11/01/22.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {

    var portfolioArray = [Portfolios]()
    var portfolioOriginalArray = [Portfolios]()

    @IBOutlet var portfoliostableView: UITableView!

// MARK: View lifecycle methods.
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let longTitleLabel = UILabel()
        longTitleLabel.text = "Portfolios"
        longTitleLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 36.0)
        let leftItem = UIBarButtonItem(customView: longTitleLabel)
        self.tabBarController?.navigationItem.leftBarButtonItem = leftItem
        navigationController?.additionalSafeAreaInsets.top = 30
        
        portfolioArray=loadJson(filename: "portfolios") ?? [Portfolios]()
        portfolioOriginalArray=portfolioArray
        // Do any additional setup after loading the view.
    }
    
// MARK: Convert portfolio values to currency
    func currencyConverter(_ price: Double) -> String{
        
        let myDouble = price
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale.current
        let priceString = currencyFormatter.string(from: NSNumber(value: myDouble))!
        print(priceString)
        
            let numberFormatter = NumberFormatter()
            numberFormatter.groupingSeparator = "'"
            numberFormatter.groupingSize = 3
            numberFormatter.usesGroupingSeparator = true
            numberFormatter.decimalSeparator = "."
            numberFormatter.numberStyle = .decimal
            numberFormatter.maximumFractionDigits = 2
        return numberFormatter.string(from: price as NSNumber)!
        }
    
// MARK: Load json file
    func loadJson(filename fileName: String) -> [Portfolios]? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(ResponseData.self, from: data)
                return jsonData.portfolios
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }

// MARK: Table view delegate and datasource methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return portfolioArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CustomCell = tableView.dequeueReusableCell(withIdentifier: "PortfoliosTableCell", for: indexPath) as! CustomCell
        let portfolioDict = portfolioArray[indexPath.row]
        cell.nameLabel.text=portfolioDict.name
        cell.idLabel.text=portfolioDict.id
        cell.currencyLabel.text=" "+portfolioDict.currency+"   "
        let str=NSDecimalNumber(string: portfolioDict.value)
        cell.valueLabel.text=currencyConverter(Double(str))
        switch portfolioDict.currency {
        case "CHF":
            cell.currencyLabel.backgroundColor=UIColor(red: 169/255, green: 138/255, blue: 118/255, alpha: 1.0)
        case "GBP":
            cell.currencyLabel.backgroundColor=UIColor(red: 0/255, green: 116/255, blue: 205/255, alpha: 1.0)
        case "USD":
            cell.currencyLabel.backgroundColor=UIColor(red: 44/255, green: 64/255, blue: 90/255, alpha: 1.0)
        default:
            cell.currencyLabel.backgroundColor=UIColor.gray
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
// MARK: Search bar delegate methods
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        portfolioArray=portfolioOriginalArray
        portfolioArray = searchText.isEmpty ? portfolioArray : portfolioArray.filter { $0.name.contains(searchText) }
        
        if searchBar.text?.count == 0 {

                viewDidLoad()
                    DispatchQueue.main.async {
                        searchBar.resignFirstResponder()
                    }

                }
        
        portfoliostableView.reloadData()
    }
    
}

// MARK: Json decoder porfolio model
struct ResponseData: Decodable {
    var portfolios: [Portfolios]
}
struct Portfolios :Decodable {
    var id = String()
    var name = String()
    var currency = String()
    var value = String()
    
}


